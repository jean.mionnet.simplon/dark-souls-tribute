// FONCTION PAUSE
const pause = () => {
  $('video').get(0).pause();
  $('video').get(1).pause();
  $('video').get(2).pause();
}

// SWIPER INITIALIZE
let swiper = new Swiper('.swiper-container', {
    direction: 'vertical',
    on: {
      slideChange: () => {
        // PAUSE VIDEOS
        pause();
        // DYNAMIC ACTIVE ANCHORS
        if(swiper.activeIndex == 0) {
          $("#home").addClass("active");
          $("#hidetaka").removeClass("active");
          $("#ds1").removeClass("active");
          $("#ds2").removeClass("active");
          $("#ds3").removeClass("active");
          $("#creators").removeClass("active");
        } else if (swiper.activeIndex == 1) {
          $("#home").removeClass("active");
          $("#hidetaka").addClass("active");
          $("#ds1").removeClass("active");
          $("#ds2").removeClass("active");
          $("#ds3").removeClass("active");
          $("#creators").removeClass("active");
        } else if (swiper.activeIndex == 2) {
          $("#home").removeClass("active");
          $("#hidetaka").removeClass("active");
          $("#ds1").addClass("active");
          $("#ds2").removeClass("active");
          $("#ds3").removeClass("active");
          $("#creators").removeClass("active");
        } else if (swiper.activeIndex == 3) {
          $("#home").removeClass("active");
          $("#hidetaka").removeClass("active");
          $("#ds1").removeClass("active");
          $("#ds2").addClass("active");
          $("#ds3").removeClass("active");
          $("#creators").removeClass("active");
        } else if (swiper.activeIndex == 4) {
          $("#home").removeClass("active");
          $("#hidetaka").removeClass("active");
          $("#ds1").removeClass("active");
          $("#ds2").removeClass("active");
          $("#ds3").addClass("active");
          $("#creators").removeClass("active");
        } else if (swiper.activeIndex == 5) {
          $("#home").removeClass("active");
          $("#hidetaka").removeClass("active");
          $("#ds1").removeClass("active");
          $("#ds2").removeClass("active");
          $("#ds3").removeClass("active");
          $("#creators").addClass("active");
        }
        if(swiper.activeIndex == 2 || swiper.activeIndex == 3 || swiper.activeIndex == 4) {
          $("#games").addClass("active");
        } else {
          $("#games").removeClass("active");
        }
      },
    }
});

//ANCHORS -> SLIDE
$("#home").click(() => {
  swiper.slideTo(0);
})
$("#hidetaka").click(() => {
  swiper.slideTo(1);
})
$("#ds1").click(() => {
  swiper.slideTo(2);
})
$("#ds2").click(() => {
  swiper.slideTo(3);
})
$("#ds3").click(() => {
  swiper.slideTo(4);
})
$("#creators").click(() => {
  swiper.slideTo(5);
})

// ANCHORS MOBILE
$("#home_mobile").click(() => {
  swiper.slideTo(0);
  // AFFICHE LE MENU DEROULANT
  $("#modal_menu").toggleClass("active");
  // CHANGE LA CLASSE DE LA NAVBAR
  $("#nav").toggleClass("mobile");
  // CHANGE L'ICONE DE MENU DEROULANT
  $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
})
$("#hidetaka_mobile").click(() => {
  swiper.slideTo(1);
  $("#modal_menu").toggleClass("active");
  $("#nav").toggleClass("mobile");
  $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
})
$("#ds1_mobile").click(() => {
  swiper.slideTo(2);
  $("#modal_menu").toggleClass("active");
  $("#nav").toggleClass("mobile");
  $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
})
$("#ds2_mobile").click(() => {
  swiper.slideTo(3);
  $("#modal_menu").toggleClass("active");
  $("#nav").toggleClass("mobile");
  $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
})
$("#ds3_mobile").click(() => {
  swiper.slideTo(4);
  $("#modal_menu").toggleClass("active");
  $("#nav").toggleClass("mobile");
  $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
})
$("#creators_mobile").click(() => {
  swiper.slideTo(5);
    $("#modal_menu").toggleClass("active");
    $("#nav").toggleClass("mobile");
    $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
})

// ON READY
$(document).ready(() => {
  // AFFICHE LE TITRE DU SITE WEB
  setTimeout(() => {
    $("#brand").fadeIn();
  }, 1000);
  if(window.innerWidth > 768 ) {
    // AFFICHE L'ASTUCE D'UTILISATION
    setTimeout(() => {
      $("#guide").fadeIn();
      $("#guide").css('display', 'block');
    }, 2500);
    // SUPPRIME L'ASTUCE D'UTILISATION
    setTimeout(() => {
      $("#guide").fadeOut();
    }, 6000)
  }
})

// CHANGE LE BOUTON BURGER A L'OUVERTURE DU MENU DEROULANT
$("#burger").click(() => {
  $("#modal_menu").toggleClass("active");
  $("#nav").toggleClass("mobile");

  if ($("#modal_menu").hasClass("active")) {
      $("#burger").html('<svg height="40" viewBox="0 0 329.26933 329" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>');
    } else {
      $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#f9f8f8" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#f9f8f8" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
  }
})
